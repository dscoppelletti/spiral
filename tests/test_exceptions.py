import http.client
import unittest
from src.dscoppelletti_spiral import ApplicationError

class ApplicationErrorTest(unittest.TestCase):

    def test_error(self):
        languages = [ ]
        ex = ApplicationError(__name__, 'ApplicationError', { 'key1' : 'value1',
            'key2' : 'value2' },  http.HTTPStatus.GONE, 'path')
        self.assertEqual(ex.get_message(languages),
            'Sample error with key1=value1 and key2=value2')

        body = ex.get_body(languages)
        self.assertEqual(body['statusCode'], http.HTTPStatus.GONE)
        self.assertEqual(body['error'], http.client.responses[http.HTTPStatus.GONE])
        self.assertEqual(body['exception'], type(ex).__name__)
        self.assertEqual(body['path'], 'path')

        resp = ex.get_response(languages)
        self.assertEqual(resp['statusCode'], http.HTTPStatus.GONE)
        self.assertEqual(resp['isBase64Encoded'], False)
