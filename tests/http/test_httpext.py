import unittest
from src.dscoppelletti_spiral.http import HttpExt

class HttpExtTest(unittest.TestCase):

    def test_accept_language(self):
        headers = { HttpExt.HEADER_LOCALE : 'it; q=0.7, es; q=0.5, en' }
        v = HttpExt.accept_language(headers)
        self.assertListEqual([ 'en_US.ISO8859-1', 'it_IT.ISO8859-1',
            'es_ES.ISO8859-1' ], v)

    def test_get_pair(self):
        headers = { HttpExt.HEADER_APPL : 'test;1' }
        (name, ver) = HttpExt.get_header_pair(headers, HttpExt.HEADER_APPL, ';')
        self.assertEqual(name, 'test')
        self.assertEqual(ver, '1')
