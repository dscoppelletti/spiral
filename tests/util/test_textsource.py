import unittest
from src.dscoppelletti_spiral.util import TextSource

class TextSourceTest(unittest.TestCase):

    def test_textsource(self):
        text = TextSource(__name__, [ 'en_US' ])
        self.assertEqual(text['greeting'], 'Hi')
        self.assertEqual(text['lang'], 'English')
        self.assertEqual(text['standard'], 'standard')
