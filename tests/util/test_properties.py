import unittest
from src.dscoppelletti_spiral.util import Properties
from src.dscoppelletti_spiral.util import Resource

class TestProperties(unittest.TestCase):

    def test_load(self):
        prop = Properties()
        with Resource.get_stream('tests.util', 'test.properties') as stream:
            prop.load(stream)

        expected = { 'key1' : 'value1',
            'key2' : '''value that continues in the next line - \
This is from the next line''',
            'key3' : 'value that does not continue in the next line ' }
        self.assertDictEqual(prop, expected)

    def test_default(self):
        prop = Properties({ 'key1' : 'overridden',
            'key4' : 'default' })
        with Resource.get_stream('tests.util', 'test.properties') as stream:
            prop.load(stream)
        self.assertEqual(prop['key1'], 'value1')
        self.assertEqual(prop['key4'], 'default')
        self.assertIn('key2', prop)
        self.assertNotIn('key4', prop)
        self.assertNotIn('key5', prop)
