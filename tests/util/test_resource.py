import unittest
from src.dscoppelletti_spiral.util import Resource

class TestResource(unittest.TestCase):

    def test_found(self):
        with Resource.get_stream('tests.util', 'test.properties') as stream:
            self.assertIsNotNone(stream)

    def test_notfound(self):
        with self.assertRaises(IOError):
            with Resource.get_stream('tests.util', 'notfound.dat'):
                pass
