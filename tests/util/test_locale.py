import unittest
from src.dscoppelletti_spiral.util import Locale

class LocaleTest(unittest.TestCase):

    def test_locale(self):
        locale = Locale('it_it.utf8@euro')
        self.assertEqual(locale.code, 'it_IT.UTF-8')
        self.assertEqual(locale.language, 'it')
        self.assertEqual(locale.script, 'UTF-8')
        self.assertEqual(locale.country, 'IT')
        self.assertEqual(locale.variant, '')

    def test_expand(self):
        locale = Locale('it_it.utf8@euro')
        v = [ c for c in locale.expand() ]
        self.assertListEqual([ 'it_UTF-8_IT', 'it_UTF-8', 'it_IT', 'it' ], v)
