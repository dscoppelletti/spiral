dscoppelletti_spiral.util - HTTP services
*****************************************

.. automodule:: dscoppelletti_spiral.http

Class Authorizer
================

.. autoclass:: dscoppelletti_spiral.http.Authorizer
   :show-inheritance:
   :members:

Class HttpExt
=============

.. autoclass:: dscoppelletti_spiral.http.HttpExt
   :show-inheritance:
   :members:


