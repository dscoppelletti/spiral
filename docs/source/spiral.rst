dscoppelletti_spiral - Spiral Library
*************************************

.. automodule:: dscoppelletti_spiral

Exception ApplicationError
==========================

.. autoexception:: dscoppelletti_spiral.ApplicationError
   :show-inheritance:
   :members:
   
.. _`Amazon API Gateway`:
   http://docs.aws.amazon.com/apigateway/latest/developerguide
.. _`AWS Lambda`: http://docs.aws.amazon.com/lambda/latest/dg
.. _`Spring Boot`: http://projects.spring.io/spring-boot
