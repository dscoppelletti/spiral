dscoppelletti_spiral.util - Miscellaneous utility classes
*********************************************************

.. automodule:: dscoppelletti_spiral.util

Class Locale
============

.. autoclass:: dscoppelletti_spiral.util.Locale
   :show-inheritance:
   :members:
   
Class Properties
================

.. autoclass:: dscoppelletti_spiral.util.Properties
   :show-inheritance:
   :members:
   :special-members: __delitem__, __getitem__, __setitem__

Class Resource
==============

.. autoclass:: dscoppelletti_spiral.util.Resource
   :show-inheritance:
   :members:

Class TextSource
================

.. autoclass:: dscoppelletti_spiral.util.TextSource
   :show-inheritance:
   :members:
   :special-members: __getitem__
   
.. _`Accessing Data Files at Runtime`:
   http://setuptools.readthedocs.io/en/latest/setuptools.html#accessing-data-files-at-runtime  
.. _`java.util.Properties`:
   http://docs.oracle.com/javase/8/docs/api/java/util/Properties.html
 