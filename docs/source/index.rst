.. Spiral Library documentation master file, created by
   sphinx-quickstart on Tue Jun 27 10:14:42 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Spiral Library's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   spiral
   spiral-util
   spiral-http

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
