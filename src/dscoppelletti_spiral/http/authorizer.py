# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class Authorizer(object):
    """``Authorizer`` component for Amazon API Gateway implemented by an AWS
    Lambda function.

    :param event: Event.
    :type event:  A :class:`dict` that complies the *Input to an Amazon API
                  Gateway Lambda Authorizer*.

    .. seealso:: * `Amazon API Gateway`_
                 * `AWS Lambda`_
    .. versionadded:: 1.0.0
    """

    ACTION_INVOKE = 'execute-api:Invoke'
    """Action ``Invoke``.
    
    :type: :class`str`
    """

    EFFECT_ALLOW = 'Allow'
    """Effect ``Allow``.

    :type: :class:`str`
    """

    EFFECT_DENY = 'Allow'
    """Effect ``Allow``.

    :type: :class:`str`
    """

    INPUTTYPE_REQUEST = 'REQUEST'
    """Input type ``REQUEST``.

    :type: :class:`str`
    """

    POLICY_VERSION = '2012-10-17'
    """Policy version.

    :type: :class:`str`
    """

    RESOURCE_ALL = 'arn:aws:execute-api::::/*'
    """All API resources.

    :type: :class:`str`
    """

    def __init__(self, event):
        self._event = event

    def allow(self, principal_id):
        """Builds the response to allow the access to the API resource.

        :param principal_id: The principal user identification associated with
                             the token sent by the client.
        :type principal_id:  :class:`str`
        :returns:            The dictionary.
        :rtype:              A :class:`dict` that complies the *Output from an
                             Amazon API Gateway Lambda Authorizer*.
        """
        return {
            'principalId' : principal_id,
            'policyDocument' : {
                'Version' : Authorizer.POLICY_VERSION,
                'Statement' : [
                    {
                        'Action' : Authorizer.ACTION_INVOKE,
                        'Effect' : Authorizer.EFFECT_ALLOW,
                        'Resource' : self._get_resource()
                    }
                ]
            }
        }

    @staticmethod
    def deny():
        """Builds the response to deny the access to the API resource.

         :returns: The dictionary.
         :rtype:   A :class:`dict` that complies the *Output from an Amazon API
                   Gateway Lambda Authorizer*.
         """
        return {
            'policyDocument' : {
                'Version' : Authorizer.POLICY_VERSION,
                'Statement' : [
                    {
                        'Action' : Authorizer.ACTION_INVOKE,
                        'Effect' : Authorizer.EFFECT_DENY,
                        'Resource' : Authorizer.RESOURCE_ALL
                    }
                ]
            }
        }

    def _get_resource(self):
        """Gets the resource.

        :returns: Resource ARN.
        :rtype:   :class:`str`
        """
        # It seems that methodArn has the apiId replaced by name of the AWS
        # CloudFormation stack hence I have to rebuild the resource.
        res = self._event['methodArn']
        region = res.split(':')[3]

        ctx = self._event['requestContext']
        res = 'arn:aws:execute-api:' + region + ':' + \
              ctx['accountId'] + ':' + ctx['apiId'] + '/' + \
              ctx['stage'] + '/' + self._event['httpMethod'] + \
             self._event['resource']
        return res