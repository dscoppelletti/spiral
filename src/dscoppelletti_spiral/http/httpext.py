# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import locale

class HttpExt(object):
    """Operations on HTTP protocol.

    .. versionadded:: 1.0.0
    """

    AUTH_BEARER = 'Bearer'
    """Authorization type ``Bearer``.

    :type: :class:`str`
    """

    CONTENTTYPE_JSON = 'application/json'
    """JSON content type.

    :type: :class:`str`
    """
    HEADER_APPL = 'X-scoppelletti-appl'
    """Header reporting the client application name and version.

    :type: :class:`str`
    """

    HEADER_AUTH = 'Authorization'
    """Header ``Authorization``.

     :type: :class:`str`
    """

    HEADER_CONTENTTYPE = 'Content-Type'
    """Header ``Accept-Language``.

    :type: :class:`str`
    """

    HEADER_LOCALE = 'Accept-Language'
    """Header ``Accept-Language``.

    :type: :class:`str`
    """

    HEADER_OS = 'X-scoppelletti-os'
    """Header reporting the client OS name and version.

    :type: :class:`str`
    """

    def __init__(self):
        raise NotImplementedError

    @staticmethod
    def get_header(headers, key):
        """Returns the value of an header.

        :param headers: Headers.
        :type headers:  :class:`dict`
        :param key:     Key of the header.
        :type key:      :class:`str`
        :returns:       The value. May be ``None``.
        :rtype:         :class:`str`
        """
        if not key:
            raise ValueError('Argument key is null.')
        if not headers:
            return None
        # Headers must be case-insensitve
        key = key.lower()
        for (k, v) in headers.items():
            if k.lower() == key:
                return v
        return None

    @staticmethod
    def get_header_pair(headers, key, sep = None):
        """Returns the value pair of an header.

        :param headers: Headers.
        :type headers:  :class:`dict`
        :param key:     Key of the header.
        :type key:      :class:`str`
        :param sep:     Value separator. May be ``None``.
        :type sep:      :class:`str`
        :returns:       The pair.
        :rtype:         :class:`tuple`
        """
        value = HttpExt.get_header(headers, key)
        if not value:
            raise ValueError('Header {0} not set.'.format(key))

        items = value.split(sep)
        if not items or len(items) < 2:
            raise ValueError('Header {0}={1} not valid.'.format(key, value))
        return (items[0].strip(), items[1].strip())

    @staticmethod
    def accept_language(headers):
        """Builds the list of preferred languages from the value of the
        ``Accept-Language`` header.

        :param headers: Headers. May be ``None``.
        :type headers:  :class:`dict`
        :returns:       The list of preferred languages sorted by descending
                        quality value.
        :rtype:         List of :class:`str` items

        .. seealso:: :rfc:`2616#section-14.4`
        """
        value = HttpExt.get_header(headers, HttpExt.HEADER_LOCALE)
        if not value:
            return [ ]

        lang_map = { }
        langs = value.split(',')
        for lang in langs:
            pos = lang.find(';')
            if pos >= 0:
                qv = lang[pos + 1:]
                lang = lang[:pos]
            else:
                qv = ''

            lang = lang.strip().lower().replace('-', '_')
            if not lang:
                continue

            pos = qv.find('=')
            if pos >= 0:
                qv = qv[pos + 1:]
            else:
                qv = '1.0'
            try:
                q = float(qv)
            except ValueError:
                q = 1.0

            if lang not in lang_map:
                lang_map[lang] = q

        lang_list = sorted(lang_map.keys(), key = lambda k : lang_map[k],
            reverse = True)
        return [ locale.normalize(x) for x in lang_list ]
