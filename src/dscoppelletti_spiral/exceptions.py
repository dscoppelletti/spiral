# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals
import http.client
import json
import time
from io import StringIO
from .http import HttpExt
from .util import TextSource

class ApplicationError(Exception):
    """Application exception.

    :param domain:     Name of the :dfn:`domain` of the localized texts. Should
                       be the name of a package; if you specify the name of a
                       module, it uses the package containing the module.
    :type domain:      :class:`str`
    :param code:       Code of the message.
    :type code:        :class:`str`
    :param args:       Dictionary of the arguments of the message. May be
                       ``None``.
    :type args:        :class:`dict`
    :param statuscode: Must be a valid integer constant like
                       ``httplib.NOT_FOUND``. May be ``None``.
    :type statuscode:  :class:`int`
    :param path:       Path of the resource. May be ``None``.
    :type path:        :class:`str`
    :param cause:      The source exception. May be ``None``.
    :type cause:       :class:`exceptions.Exception`

    .. seealso:: * :class:`dscoppelletti_spiral.util.TextSource`
                 * :meth:`str.format`
    .. versionadded:: 1.0.0
    """

    def __init__(self, domain, code, args = None, statuscode = None,
                 path = None, cause = None):
        self._domain = domain
        self._code = code
        self._args = args
        self._statuscode = statuscode
        self._path = path
        self._cause = cause

        if isinstance(cause, ApplicationError):
            self._timestamp = cause._timestamp
            if not self._statuscode:
                self._statuscode = cause._statuscode
            if not self._path:
                self._path = cause._path
        else:
            self._timestamp = time.time() * 1000

    @property
    def domain(self):
        """Domain of the localized texts.

        :type: :class:`str` (read only)
        """
        return self._domain

    @property
    def code(self):
        """Code of the message.

        :type: :class:`str` (read only)
        """
        return self._code

    @property
    def arguments(self):
        """Dictionary of the arguments of the message. May be ``None``.

        :type: :class:`dict` (read only)
        """
        return self._args

    @property
    def statuscode(self):
        """The HTTP status code. May be ``None``.

        :type: :class:`int` (read only)
        """
        return self._statuscode

    @property
    def path(self):
        """Path of the resource. May be ``None``.

        :type: :class:`str` (read only)
        """
        return self._path

    @property
    def cause(self):
        """The source exception. May be ``None``.

        :type: :class:`exceptions.Exception` (read only)
        """
        return self._cause

    @property
    def timestamp(self):
        """The timestamp of the exception. Represented by a millisecond value
        that is an offset from the :dfn:`Epoch`.

        :type: :class:`long` (read only)
        """
        return self._timestamp

    def __str__(self):
        buf = StringIO.StringIO()
        buf.write(type(self).__name__)
        buf.write('(')
        buf.write('code=[')
        buf.write(self._domain)
        buf.write(']')
        buf.write(self._code)
        if self._statuscode:
            buf.write(', statusCode=')
            buf.write(self._statuscode)
        if self._path:
            buf.write(', path=')
            buf.write(self._path)
        if self._cause:
            buf.write(', cause=')
            buf.write(str(self._cause))
        buf.write(')')
        return buf.getvalue()

    def get_message(self, languages = None):
        """Returns the localized message.

        :param languages: List of the preferred languages sorted by descending
                          priority. May be ``None``.
        :type languages:  List of :class:`str` items
        :returns:         The message.
        :rtype:           :class:`str`
        """
        textsource = TextSource(self._domain, languages)
        msg = textsource[self._code]
        if self._args:
            msg = msg.format(**self._args)
        return msg

    def get_body(self, languages = None):
        """Builds a dictionary that models the exception.

        The model mimics the JSON object returned by the default
        ``ErrorController`` implementation in Spring Boot 1.3.4:

        ========== ========================================================
        key        Value
        ========== ========================================================
        message    The localized message.
        statusCode The HTTP status code (optional).
        error      Description of the HTTP status code (optional).
        exception  Class of the original exception.
        path       Path of the resource (optional).
        timestamp  Timestamp of the exception. Represented by a millisecond
                   value that is an offset from the :dfn:`Epoch`.
        ========== ========================================================

        :param languages: List of the preferred languages sorted by descending
                          priority. May be ``None``.
        :type languages:  List of :class:`str` items
        :returns:         The dictionary.
        :rtype:           :class:`dict`

        .. seealso:: * :meth:`dscoppelletti_spiral.ApplicationError.get_message`
                     * `Spring Boot`_
        """
        body = { 'message' : self.get_message(languages),
            'timestamp' : self._timestamp }
        if self._statuscode:
            body['statusCode'] = self._statuscode
            body['error'] = http.client.responses[self._statuscode]
        if self._cause:
            body['exception'] = type(self._cause).__name__
        else:
            body['exception'] = type(self).__name__
        if self._path:
            body['path'] = self._path
        return body

    def get_response(self, languages = None):
        """Builds a dictionary that models the exception in order to use it as
        the response for a request to an Amazon API Gateway resource.

        :param languages: List of the preferred languages sorted by descending
                          priority. May be ``None``.
        :type languages:  List of :class:`str` items
        :returns:         The dictionary.
        :rtype:           A :class:`dict` that complies the *Output Format of a
                          Lambda Function for Proxy Integration*:

        .. seealso:: * :meth:`dscoppelletti_spiral.ApplicationError.get_body`
                     * `Amazon API Gateway`_
                     * `AWS Lambda`_
        """
        if self._statuscode:
            statuscode = self._statuscode
        else:
            statuscode = http.client.INTERNAL_SERVER_ERROR
        resp = { 'statusCode' : statuscode,
            'body' : json.dumps(self.get_body(languages)),
            'headers' : {
                HttpExt.HEADER_CONTENTTYPE : HttpExt.CONTENTTYPE_JSON
            },
            'isBase64Encoded' : False }
        return resp
