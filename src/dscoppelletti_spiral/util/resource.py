# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pkg_resources

class Resource(object):
    """Resource (or data files) installed in a package.
    
    .. versionadded:: 1.0.0
    """
    
    def __init__(self):
        raise NotImplementedError
    
    @staticmethod
    def get_stream(pkg_name, resource_name):
        """Returns a readable file-like object for a resource.
    
        :param pkg_name:      Name of the package containing the resource. If
                              you specify the name of a module, then it uses the
                              package containing the module.
        :type pkg_name:       :class:`str`
        :param resource_name: Name of the resource.
        :type resource_name:  :class:`str`
        :returns:             The stream.
        :raises:              :exc:`exceptions.ImportError`,
                              :exc:`exceptions.IOError`
    
        .. seealso:: `Accessing Data Files at Runtime`_
        """
        # - PyDev for Eclipse 5.8.0
        # In the virtual environment I can use the resource_stream function from
        # the pkg_resources module, but PyDev does not find it, so I have to use
        # a ResourceManager object.
        res_mgr = pkg_resources.ResourceManager()
        return res_mgr.resource_stream(pkg_name, resource_name)
