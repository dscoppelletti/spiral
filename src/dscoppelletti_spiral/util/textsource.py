# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import threading
import weakref
from . import Locale
from . import Properties
from . import Resource

_logger = logging.getLogger(__name__)

class TextSource(object):
    """Source of localized texts.

    A ``TextSource`` object looks for localized texts in the resources within
    the ``domain`` package. The resources must be properties files as defined by
    the the ``load`` method of the ``Properties`` JDK class. Each language in
    the ``languages`` list is expanded in a list of :abbr:`L10N (localization)`
    :dfn:`classifiers` as defined by the ``expand`` method of the ``Locale``
    class.

    Each resource must be named :samp:`textsource_{classifier}.properties`.
    For example, if a language is ``it_IT``, the names of the corresponding
    :dfn:`localized resources` are:

    #. ``textsource_it_IT.properties``
    #. ``textsource_it.properties``

    If a localized text is not provided by any of the localized resources, the
    ``TextSource`` object searches in the :dfn:`standard resource`
    ``textsource.properties`` within the ``domain`` package.

    :param domain:    Name of the :dfn:`domain` of the localized texts. Should
                      be the name of a package; if you specify the name of a
                      module, it uses the package containing the module.
    :type domain:     :class:`str`
    :param languages: List of the preferred languages sorted by descending
                      priority. May be ``None``
    :type languages:  List of :class:`str` items

    .. note:: The ``TextSource`` class caches the loaded resources to better
              performance.

    .. seealso:: * :meth:`dscoppelletti_spiral.http.HttpExt.accept_language`
                 * :meth:`dscoppelletti_spiral.util.Locale.expand`
                 * :class:`dscoppelletti_spiral.util.Properties`
                 * :meth:`dscoppelletti_spiral.util.Resource.get_stream`
                 * `java.util.Properties`_
    .. versionadded:: 1.0.0
    """
    _sync_root = threading.RLock()
    _cache = weakref.WeakValueDictionary()

    def __init__(self, domain, languages = None):
        self._domain = domain
        self._languages = [ ] if languages is None else languages
        self._props = None

    def __getitem__(self, key):
        """Returns the localized text for a key.

        :param key: The key.
        :type key:  :class:`str`
        :returns:   The value.
        :rtype:     :class:`str`
        :raises:    :exc:`exceptions.KeyError`, :exc:`exceptions.TypeError`
        """
        with TextSource._sync_root:
            if not self._props:
                self._props = self._load()
        for props in self._props:
            try:
                value = props[key]
            except KeyError:
                pass
            else:
                return value
        raise KeyError('Key {0} not found'.format(key))

    def _load(self):
        """Loads the properties files where to search for localized texts.

        :returns: List of the resources.
        :rtype:   List of :class:`dscoppelletti_spiral.util.Properties` items
        """
        v = [ ]
        for lang in self._languages:
            locale = Locale(lang)
            for c in locale.expand():
                res_name = 'textsource' + '_' + c + '.properties'
                props = self._get_resource(res_name)
                if props:
                    v.append(props)
        props = self._get_resource('textsource.properties')
        if props:
            v.append(props)
        return v

    def _get_resource(self, res_name):
        """Loads a resource.

        :param res_name: Name of the resource.
        :type res_name:  :class:`str`
        :returns:        The resource. May be ``None``.
        :rtype:          :class:`dscoppelletti_spiral.util.Properties`
        """
        key = (self._domain, res_name)
        props = TextSource._cache.get(key, None)
        if props:
            return props
        try:
            with Resource.get_stream(self._domain, res_name) as stream:
                props = Properties()
                props.load(stream)
        except ImportError:
            _logger.exception('Failed to import %s.', self._domain)
            return None
        except IOError:
            _logger.debug('Failed to read %s.%s resource.', self._domain,
                res_name)
            return None

        TextSource._cache[key] = props
        return props
