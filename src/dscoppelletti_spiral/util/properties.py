# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals
from io import StringIO

class Properties(dict):
    """Subclass of dictionary that adds the capability to load a properties file
    just like the ``load`` method of the ``Properties`` JDK class.

    :param default: A dictionary that contains default values for any keys not
                    found in this property list. May be ``None``.
    :type default:  :class:`dict`

    .. seealso:: `java.util.Properties`_
    .. versionadded:: 1.0.0
    """

    def __init__(self, default = None):
        super(Properties, self).__init__()
        self._default = default

    def get(self, key, default = None):
        """Returns the value for a key.

        #. If the key is in this dictionary, returns the value for the key.
        #. If this dictionary has a default values dictionary, calls the same
           method of the default values dictionary with the same arguments.
        #. If a default value is specified, returns the default value.
        #. Returns ``None``.

        :param key:     The key.
        :type key:      :class:`str`
        :param default: The default value. May be ``None``.
        :returns:       The value.
        :raises:        :exc:`exceptions.TypeError`
        """
        k = self._check_key(key)
        if k not in self:
            if self._default:
                return self._default.get(k, default)
        return dict.get(self, k, default)

    def __getitem__(self, key):
        """Returns the value for a key.

        #. If the key is in this dictionary, returns the value for the key.
        #. If this dictionary has a default values dictionary, calls the same
           method of the default values dictionary with the same arguments.
        #. Raises ``KeyError``.

        :param key: The key.
        :type key:  :class:`str`
        :returns:   The value.
        :raises:    :exc:`exceptions.KeyError`, :exc:`exceptions.TypeError`
        """
        k = self._check_key(key)
        if k not in self:
            if self._default:
                # The following may raise KeyError
                return self._default[k]
            raise KeyError('Key {0} not found'.format(k))
        return dict.__getitem__(self, k)

    def __setitem__(self, key, value):
        """Sets the item for a key.

        :param key:   The key.
        :type key:    :class:`str`
        :param value: The value.
        :raises:      :exc:`exceptions.TypeError`
        """
        k = self._check_key(key)
        dict.__setitem__(self, k, value)

    def __delitem__(self, key):
        """Removes a key from this dictionary.

        :param key: The key.
        :type key:  :class:`str`
        :raises:    :exc:`exceptions.KeyError`, :exc:`exceptions.TypeError`
        """
        k = self._check_key(key)
        dict.__delitem__(self, k)

    def _check_key(self, key):
        """Checks the type of a key.

        :param key: The key.
        :type key:  :class:`str`
        :returns:   The key.
        :rtype:     :class:`str`
        :raises:    :exc:`exceptions.TypeError`
        """
        if isinstance(key, str):
            k = key
        else:
            raise TypeError("Key must be a string.")
        return k

    def load(self, stream):
        """Reads the items from a stream.

        :param stream: The stream. Is assumed to use the UTF-8 character
                       encoding.
        """
        reader = _LineReader(stream)
        for line in reader.readlines():
            key, value = self._split_line(line)
            key = self._convert(key)
            value = self._convert(value)
            dict.__setitem__(self, key, value)

    def _split_line(self, line):
        """Splits a line in a (key, value) pair.

        :param line: The line.
        :type line:  :class:`str`
        :returns:    The (key, value) pair.
        :rtype:      Pair of :class:`str` items
        """
        limit = len(line)
        key_len = 0
        value_start = limit
        has_sep = False
        backslash = False

        while key_len < limit:
            c = line[key_len]
            if not backslash:
                if c == '=' or c == ':':
                    value_start = key_len + 1
                    has_sep = True
                    break
                if c == ' ' or c == "\t" or c == "\f":
                    value_start = key_len + 1
                    break
            if c == "\\":
                backslash = not backslash
            else:
                backslash = False
            key_len += 1

        while value_start < limit:
            c = line[value_start]
            if c != ' ' and c != "\t" and c != "\f":
                if not has_sep and (c == '=' or c == ':'):
                    has_sep = True
                else:
                    break
            value_start += 1
        if value_start < limit:
            value = line[value_start:]
        else:
            value = u""

        return (line[0:key_len], value)

    def _convert(self, s):
        """Converts any special character in a string.

        :param s: The source string.
        :type s:  :class:`str`
        :returns: The converted string.
        :rtype:   :class:`str`
        :raises:  :exc:`exceptions.ValueError`
        """
        buf = StringIO()
        n = len(s)
        i = 0
        while i < n:
            c = s[i]
            i += 1
            if c != "\\" or i >= n:
                buf.write(c)
                continue

            c = s[i]
            i += 1
            if c == 'u':
                if n - i < 4:
                    raise ValueError("Malformed \\uxxxx encoding.")
                x = s[i:i + 4]
                i += 4
                try:
                    value = int(x, 16)
                except ValueError:
                    raise ValueError("Malformed \\uxxxx encoding.")
                buf.write(unichr(value))
                continue

            if c == 't':
                c = "\t"
            elif c == 'r':
                c = "\r"
            elif c == 'n':
                c = "\n"
            elif c == 'f':
                c = "\f"
            buf.write(c)
        return buf.getvalue()

class _LineReader:
    """Reads a properties file.
    """

    def __init__(self, stream):
        """Constructor.

        :param stream: The stream.
        """
        self._stream = stream
        self._lineBuf = None

    def readlines(self):
        """Lines generator.
        """
        for line_b in self._stream:
            line = line_b.decode('utf-8')
            if line.endswith("\r\n"):
                line = line[:-2]
            elif line.endswith("\n"):
                line = line[:-1]
            line = line.lstrip()
            if line.startswith('#'):
                value = self._getvalue()
                if value:
                    yield value
                continue
            if self._endswith_backslash(line):
                self._append(line[:-1])
                continue
            self._append(line)
            value = self._getvalue()
            if value:
                yield value
        value = self._getvalue()
        if value:
            yield value

    def _getvalue(self):
        """Returns the current line.

        :returns: The line.
        :rtype:   :class:`str`
        """
        if not self._lineBuf:
            return None
        value = self._lineBuf.getvalue()
        self._lineBuf.close()
        self._lineBuf = None
        return value

    def _append(self, s):
        """Appends a string to the current line.

        :param s: The string.
        :type s:  :class:`str`
        """
        if not self._lineBuf:
            self._lineBuf = StringIO()
        self._lineBuf.write(s)

    def _endswith_backslash(self, s):
        """Returns whether a string ends with a non-quoted backslash.

        :param s: The string.
        :type s:  :class:`str`
        :returns: Returns ``True`` if the string ends with a backslash,
                  ``False`` otherwise.
        """
        backslash = False
        for c in s:
            if c == "\\":
                backslash = not backslash
            else:
                backslash = False
        return backslash
