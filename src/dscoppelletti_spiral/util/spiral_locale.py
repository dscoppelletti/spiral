# Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# - Python 2.7.10
# If I name this module "locale" I get unexpected behaviours concerning the
# collision with the standard library module "locale".

from __future__ import unicode_literals
import locale

class Locale(object):
    """Locale.

    :param code: Code of the locale.
    :type code:  :class:`str`

    .. versionadded:: 1.0.0
    """

    def __init__(self, code):
        self._code = locale.normalize(code)

        self._lang = self._code
        pos = self._lang.find('@')
        if pos >= 0:
            self._variant = self._lang[pos + 1:]
            self._lang = self._lang[:pos]
        else:
            self._variant = ''

        pos = self._lang.find('.')
        if pos >= 0:
            self._script = self._lang[pos + 1:]
            self._lang = self._lang[:pos]
        else:
            self._script = ''

        pos = self._lang.find('_')
        if pos >= 0:
            self._country = self._lang[pos + 1:]
            self._lang = self._lang[:pos]
        else:
            self._country = ''

    @property
    def code(self):
        """The code. May be empty.

        :type: :class:`str` (read only)
        """
        return self._code

    @property
    def language(self):
        """The language. May be empty.

        :type: :class:`str` (read only)
        """
        return self._lang

    @property
    def script(self):
        """The script. May be empty.

        :type: :class:`str` (read only)
        """
        return self._script

    @property
    def country(self):
        """The country. May be empty.

        :type: :class:`str` (read only)
        """
        return self._country

    @property
    def variant(self):
        """The variant. May be empty.

        :type: :class:`str` (read only)
        """
        return self._variant

    def __str__(self):
        return self._code

    def expand(self):
        """Generates the :abbr:`L10N (localization)` :dfn:`classifiers`
        corresponding to this locale.

        The classifiers may be:

        #. ``language + '_' + script + '_' + country + '_' + variant``
        #. ``language + '_' + script + '_' + country``
        #. ``language + '_' + script``
        #. ``language + '_' + country + '_' + variant``
        #. ``language + '_' + country``
        #. ``language``

        .. note:: Each classifier is appended to the list only if all its
                  attributes is specified.

        :returns: Generator of the classifiers.
        :rtype:   Generator of :class:`str` items
        """
        if self._lang and self._script and self._country and self._variant:
            yield '_'.join([self._lang, self._script, self._country,
                self._variant])
        if self._lang and self._script and self._country:
            yield '_'.join([self._lang, self._script, self._country])
        if self._lang and self._script:
            yield '_'.join([self._lang, self._script])
        if self._lang and self._country and self._variant:
            yield '_'.join([self._lang, self._country, self._variant])
        if self._lang and self._country:
            yield '_'.join([self._lang, self._country])
        if self._lang:
            yield self._lang
